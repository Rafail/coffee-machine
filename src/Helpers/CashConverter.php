<?php

namespace App\Helpers;


class CashConverter
{
    private const AVAILABLE_MONEY = [100, 50, 10, 5, 2, 1];

    public function convertArrayToInt(array $cashInArray): int
    {
        $cash = 0;
        foreach ($cashInArray as $value => $amount) {
            $cash += $value * $amount;
        }

        return $cash;
    }

    public function convertIntToArray(int $cash): array
    {
        if ($cash == 0) {
            return [0];
        }

        $cashBack = [];

        foreach (self::AVAILABLE_MONEY as $moneyValue) {
            $amount = floor($cash / $moneyValue);
            if ($amount > 0) {
                $cashBack[$moneyValue] = (int)$amount;
                $cash -= $moneyValue * $amount;
            }
        }

        return $cashBack;
    }

}
