<?php

namespace App\Controller;

use App\Entity\Invoice;
use App\Models\LastOrderModel;
use App\Models\OrderModel;
use App\Validator\OrderValidator;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends AbstractFOSRestController
{
    /**
     * @Rest\Post("/order", name="create_new_order")
     */
    public function newAction
    (
        Request $request,
        OrderValidator $validator,
        OrderModel $model
    )
    {
        $data = json_decode($request->getContent(), true);
        $errors = $validator->validate($data);
        if (count($errors) !== 0) {
            return $this->json($errors, Response::HTTP_BAD_REQUEST);
        }

        $model->load($data);
        $model->create();

        if ($errors = $model->getErrors()) {
            return $this->json($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->json([
            'success' => 'Enjoy your drinks!',
            'your order' => $model->getOrderDrinks(),
            'cashBack' => $model->getCashBack(),
        ],
            Response::HTTP_CREATED
        );
    }


    /**
     * @Rest\Get("/order/last", name="get_last_order")
     */
    public function lastOrderAction(LastOrderModel $model)
    {
        $result = $model->getResult();
        if ($errors = $model->getErrors()) {
            return $this->json($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->json($result, Response::HTTP_OK);
    }

}
