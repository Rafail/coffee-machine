<?php

namespace App\Controller;


use App\Entity\SoftDrink;
use App\Forms\SoftDrinkForm;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SoftDrinkController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/soft-drinks", name="soft_drinks_list")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $softDrinks = $em->getRepository(SoftDrink::class)
            ->findAllAvailable();

        return $this->json($softDrinks);
    }

    /**
     * @Rest\Get("/soft-drinks/not-available", name="soft_drinks_list_of_not_available")
     */
    public function listOfNotAvailableAction()
    {
        $em = $this->getDoctrine()->getManager();
        $softDrinks = $em->getRepository(SoftDrink::class)
            ->findAllNotAvailable();

        return $this->json($softDrinks);
    }

    /**
     * @Rest\Post("/soft-drinks", name="soft_drinks_add")
     */
    public function addAction(Request $request, ValidatorInterface $validator)
    {
        $form = $this->createForm(SoftDrinkForm::class);
        $form->submit(json_decode($request->getContent(), 1));
        $newSoftDrink = $form->getData();

        $errors = $validator->validate($newSoftDrink, null, ['new', 'Default']);

        if (count($errors) > 0) {
            return $this->json([$errors], Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();
        $newSoftDrink->setIsAvailable($newSoftDrink->getAmount() > 0);
        $em->persist($newSoftDrink);
        $em->flush();

        return $this->json($newSoftDrink, Response::HTTP_CREATED);
    }

    /**
     * @Rest\Put("/soft-drinks/{id}", name="soft_drinks_update", requirements={"id"="\d+"})
     */
    public function updateAction($id, Request $request, ValidatorInterface $validator)
    {
        $em = $this->getDoctrine()->getRepository(SoftDrink::class);
        $softDrink = $em->findOneBy(['id' => $id]);

        if (!$softDrink) {
            return $this->json(['errors' => ['message' => "Soft drink with id = $id not found"]],
                Response::HTTP_BAD_REQUEST);
        }

        $form = $this->createForm(SoftDrinkForm::class);
        $form->submit(json_decode($request->getContent(), 1));
        $newSoftDrink = $form->getData();

        $errors = $validator->validate($newSoftDrink);

        if (count($errors) > 0) {
            return $this->json([$errors], Response::HTTP_BAD_REQUEST);
        }


        $softDrink->setName($newSoftDrink->getName());
        $softDrink->setAmount($newSoftDrink->getAmount());
        $softDrink->setCost($newSoftDrink->getCost());
        $softDrink->setIsAvailable($newSoftDrink->getAmount() > 0);

        $em = $this->getDoctrine()->getManager();
        $em->persist($softDrink);
        $em->flush();

        return $this->json($softDrink);
    }

    /**
     * @Rest\Delete("/soft-drinks/{name}", name="soft_drinks_delete")
     */
    public function deleteByNameAction($name)
    {
        $em = $this->getDoctrine()->getManager();
        $softDrink = $em->getRepository(SoftDrink::class)
            ->findOneBy(['name' => $name]);

        if (!$softDrink) {
            return $this->json(['errors' => ['soft drink was not found']], Response::HTTP_BAD_REQUEST);
        }

        $softDrink->setIsAvailable(false);
        $em->persist($softDrink);
        $em->flush();

        return $this->json(['success' => 'The drink has been set as not available successfully!']);
    }

}
