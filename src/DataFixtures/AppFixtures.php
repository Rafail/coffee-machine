<?php

namespace App\DataFixtures;

use App\DataFixtures\CustomProviders\InvoiceItemCostProvider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\DataFixtures\CustomProviders\SoftDrinkProvider;
use Faker\Factory;
use Nelmio\Alice\Loader\NativeLoader;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $faker->addProvider(SoftDrinkProvider::class);
        $faker->addProvider(InvoiceItemCostProvider::class);
        $loader = new NativeLoader($faker);
        $objectSet = $loader->loadFile(__DIR__ . '/fixtures.yml');
        $objects = $objectSet->getObjects();

        foreach ($objects as $object) {
            $manager->persist($object);
        }

        $manager->flush();
    }
}
