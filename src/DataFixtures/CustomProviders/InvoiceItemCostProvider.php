<?php

namespace App\DataFixtures\CustomProviders;


use App\Entity\SoftDrink;

class InvoiceItemCostProvider
{
    public function getSoftDrinkCost(SoftDrink $softDrink): int
    {
        return $softDrink->getCost();
    }

}