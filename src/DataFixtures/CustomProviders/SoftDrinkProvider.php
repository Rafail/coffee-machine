<?php

namespace App\DataFixtures\CustomProviders;


class SoftDrinkProvider
{
    public function softDrinkName()
    {
        $genera = [
            'Instant black coffee',
            'Instant black coffee with milk',
            'Coffee espresso',
            'Coffee latte',
            'Coffee cappuccino',
            'Black tea',
            'Green tea',
        ];

        $key = array_rand($genera);

        return $genera[$key];
    }
}
