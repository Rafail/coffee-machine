<?php

namespace App\Repository;

use App\Entity\SoftDrink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SoftDrink|null find($id, $lockMode = null, $lockVersion = null)
 * @method SoftDrink|null findOneBy(array $criteria, array $orderBy = null)
 * @method SoftDrink[]    findAll()
 * @method SoftDrink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SoftDrinkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SoftDrink::class);
    }

    public function findAllAvailableByNames(array $names)
    {
        return $this->createQueryBuilder('soft_drink')
            ->andWhere('soft_drink.name IN (:names)')
            ->setParameter(':names', $names)
            ->andWhere('soft_drink.isAvailable = true')
            ->getQuery()
            ->execute();
    }

    public function findAllNotAvailable()
    {
        return $this->findAllByAvailable(false);
    }

    public function findAllAvailable()
    {
        return $this->findAllByAvailable(true);
    }

    private function findAllByAvailable(bool $available)
    {
        return $this->createQueryBuilder('soft_drink')
            ->andWhere('soft_drink.isAvailable = :available')
            ->setParameter(':available', $available)
            ->getQuery()
            ->execute();
    }
}
