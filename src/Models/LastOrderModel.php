<?php

namespace App\Models;


use App\Entity\Invoice;
use Doctrine\ORM\EntityManagerInterface;

class LastOrderModel
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    private $errors = [];

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getResult()
    {
        $lastOrder = $this->em->getRepository(Invoice::class)
            ->findLast();

        if (!$lastOrder) {
            $this->errors['error'][] = 'There is no any order. Please make one first.';
            return;
        }

        $items = $lastOrder->getInvoiceItems();

        $result = [];
        $result['id'] = $lastOrder->getId();
        $result['createdAt'] = $lastOrder->getCreatedAt()->format('Y-m-d H:i:s');
        $order = [];
        $totalCost = 0;

        foreach ($items as $item) {
            $softDrink = $item->getSoftDrink();
            $order[$softDrink->getName()] = isset($order[$softDrink->getName()]) ? $order[$softDrink->getName()] + 1 : 1;
            $totalCost += $item->getCost();
        }

        $result['order'] = $order;
        $result['totalCost'] = $totalCost;

        return $result;
    }

    public function getErrors(): array
    {
        return empty($this->errors['errors']) ? [] : $this->errors;
    }

}
