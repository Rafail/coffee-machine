<?php

namespace App\Models;


use App\Entity\Invoice;
use App\Entity\InvoiceItem;
use App\Entity\SoftDrink;
use App\Helpers\CashConverter;
use Doctrine\ORM\EntityManagerInterface;

class OrderModel
{
    private $data;
    /**
     * @var CashConverter
     */
    private $cashConverter;

    private $errors = [];

    private $cashBack;

    private $orderDrinks;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em, CashConverter $cashConverter)
    {
        $this->cashConverter = $cashConverter;
        $this->em = $em;
    }

    public function load(array $data)
    {
        $this->data = $data;
    }

    public function create()
    {
        $order = $this->data['softDrinks'];
        $softDrinkNames = array_keys($order);
        $cash = $this->cashConverter->convertArrayToInt($this->data['cash']);

        $softDrinks = $this->em->getRepository(SoftDrink::class)
            ->findAllAvailableByNames($softDrinkNames);

        if (!$softDrinks) {
            $this->errors['errors'][] = 'Drinks not found';
            return;
        }

        $invoice = new Invoice();
        $invoice->setCreatedAt(new \DateTime('now'));

        $this->em->persist($invoice);

        $cashBack = $cash;
        $orderDrinks = [];
        $invoiceItems = [];
        /** @var SoftDrink[] $softDrinks */
        foreach ($softDrinks as $softDrink) {
            $orderAmount = $order[$softDrink->getName()];
            $machineAmount = $softDrink->getAmount();
            $amount = $machineAmount - $orderAmount < 0 ? $machineAmount : $orderAmount;

            if ($amount > 0 && $cashBack - $softDrink->getCost() >= 0) {
                $newAmount = $softDrink->getAmount() - $amount;
                $softDrink->setAmount($newAmount);
                if ($newAmount == 0) {
                    $softDrink->setIsAvailable(false);
                }
                $this->em->persist($softDrink);

                $orderDrinks[$softDrink->getName()] = $amount;
                while ($amount > 0) {
                    $item = new InvoiceItem();
                    $item->setCost($softDrink->getCost());
                    $item->setSoftDrink($softDrink);
                    $item->setInvoice($invoice);
                    $invoiceItems[] = $item;
                    $amount--;
                    $cashBack -= $softDrink->getCost();
                }
            }
        }

        if (!$invoiceItems) {
            $this->errors['errors'][] = 'Drinks are not available or you have put in not enough cash';
            return;
        }

        foreach ($invoiceItems as $item) {
            $this->em->persist($item);
        }

        $this->em->flush();

        $this->cashBack = $this->cashConverter->convertIntToArray($cashBack);
        $this->orderDrinks = $orderDrinks;
    }

    public function getCashBack()
    {
        return $this->cashBack;
    }

    public function getOrderDrinks()
    {
        return $this->orderDrinks;
    }

    public function getErrors(): array
    {
        return empty($this->errors['errors']) ? [] : $this->errors;
    }

}
