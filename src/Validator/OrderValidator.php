<?php

namespace App\Validator;


use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints AS Assert;

class OrderValidator
{
    public function validate($data)
    {
        $validator = Validation::createValidator();

        $constraint = [new Assert\NotBlank(),
            new Assert\Collection([
                'softDrinks' => [
                    new Assert\Optional([
                        new Assert\NotBlank(),
                        new Assert\Type('array'),
                        new Assert\Count(['min' => 1]),
                    ])],
                'cash' => [new Assert\Optional([
                        new Assert\NotBlank(),
                        new Assert\Type('array'),
                        new Assert\Count(['min' => 1]),
                    ])],
            ])];

        return $validator->validate($data, $constraint);
    }

}
