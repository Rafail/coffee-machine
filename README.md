##Install
git clone git@gitlab.com:Rafail/coffee-machine.git
<br>cd coffee-machine (open application directory)
<br>composer install

##Configure docker environment
cp .env .env.local (it is optional!)
<br>docker-compose up -d --build (run docker)

##Upload fixtures
docker-compose exec app bash
<br> ./bin/console doctrine:fixtures:load

##Configuration
default web-server configured to port 8080 (http://localhost:8080)
<br> default mysql database configured to port 13306
<br> database default name is **coffee**
<br> database default user is **coffee**
<br> database default password is **secret**
<br> Now you may connect to DB **mysql -u coffee --host=127.0.0.1 -P 13306 -psecret**

##Run tests
php bin/phpunit

##Examples
**Make an order**

POST http://localhost:8080/order

Data:<br>
{
  "softDrinks": {
    "Coffee espresso": 1,
    "Black tea": 2
  },
  "cash": {
   "100": 2,
   "50": 3,
   "20": 2,
   "5": 1
  }
}

Result:<br>
{
  "success": "Enjoy your drinks!",
  "your order": {
      "Coffee espresso": 1,
      "Black tea": 2
    },
  "cashBack": {
    "100": 1,
    "50": 1,
    "10": 3,
    "2": 1,
    "1": 1
  }
}

**Cash** is amount of the different paper money and coins.
<br>**CashBack** is a change in the different paper money and coins.
In this example a customer put in 2 banknotes of 100, 3 banknotes of 50, 2 coins of 20 and 1 coin of 5.
The customer got a change: 1 banknote of 100, 1 banknote of 50, 3 coins of 10, 1 coin of 2, 1 coin of 1.
<br>**In case if drinks you are ordering are not available or you have put in not enough cash you will get an error with message "Drinks are not available or you have put in not enough cash"**

**Get last order information**

GET http://localhost:8080/order/last

Result:<br>
{
  "id": 799,
  "createdAt": "2019-03-11 09:08:19",
  "order": {
    "Coffee espresso": 2,
    "Green tea": 2
  },
  "totalCost": 354
}

**Get list of available soft drinks**

GET http://localhost:8080/soft-drinks/not-available

Result:
<br>
[
  {
    "id": 47,
    "name": "Coffee espresso",
    "amount": 1,
    "isAvailable": true,
    "cost": 42
  },
  {
    "id": 48,
    "name": "Black tea",
    "amount": 2,
    "isAvailable": true,
    "cost": 70
  }
]

**Get list of not available soft drinks**

GET http://localhost:8080/soft-drinks

Result:
<br>
[
  {
    "id": 62,
    "name": "Green tea",
    "amount": 5,
    "isAvailable": false,
    "cost": 50
  }
]

**Create a new soft drink**

POST http://localhost:8080/soft-drinks
<br>Data:
<br>
{
    "name": "Coffee mochaccino",
    "amount": 4,
    "cost": 100
}

Result:
<br>
{
  "id": 66,
  "name": "Coffee mochaccino",
  "amount": 4,
  "isAvailable": true,
  "cost": 100
}

Status **code 201** means a new soft drink was created.
<br>**If a drink amount equals to 0 the created drink will be set as not available for ordering.**

**Update soft drink**

PUT http://localhost:8080/soft-drinks/63
<br>Data:
<br>
{
  "name": "Green tea",
  "amount": 4,
  "isAvailable": true,
  "cost": 100
}

Result:
<br>
{
  "id": 63,
  "name": "Green tea",
  "amount": 4,
  "isAvailable": true,
  "cost": 100
}

**Delete(set as not available) soft drink(by name)**

DELETE http://localhost:8080/soft-drinks/Coffee mochaccino

Result:
<br>
{
  "success": "The drink has been set as not available successfully!"
}
<br>or
{
  "errors": [
    "soft drink was not found"
  ]
}

Status **code 400** means the sending data has some validation issue.

#Task description

Написать приложение "Кофейный автомат". Приложение должно уметь:
- Вести справочник товаров (чай, кофе и другие напитки) с разными ценами;
- Принимать купюры и монеты разного достоинства;
- Проверять корректность купюры или монеты;
- Позволять выбирать товар для покупки;
- Выдавать купленный товар;
- Выдавать сдачу.
- Сдача выдаётся наименьшим числом монет и/или купюр;

Дополнительно:
- Покрыть код приложения тестами;
- Визуализация процессов;
- UML и ER диаграммы;
- Проект готов к работе после выполнения composer install;
- Реализовать проект в виде REST-сервиcа.