<?php

namespace App\Tests\Controller;


use App\Entity\SoftDrink;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SoftDrinkControllerTest extends WebTestCase
{
    private $client;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        $this->client = static::createClient();

        parent::__construct($name, $data, $dataName);
    }


    public function testSoftDrinksListOfAvailable()
    {
        $this->client->request('GET', '/soft-drinks');
        $this->checkList();
    }

    public function testSoftDrinksListOfNotAvailable()
    {
        $this->client->request('GET', '/soft-drinks/not-available');
        $this->checkList(false);
    }

    private function checkList(bool $available = true)
    {
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $content = json_decode($this->client->getResponse()->getContent(), true);

        if (empty($content)) {
            return;
        }

        $this->assertInternalType('array', $content);

        foreach ($content as $item) {
            $this->assertInternalType('array', $item);
            $this->assertArrayHasKey('id', $item);
            $this->assertNotEmpty($item['id']);
            $this->assertArrayHasKey('name', $item);
            $this->assertNotEmpty($item['name']);
            $this->assertArrayHasKey('amount', $item);
            $this->assertNotNull($item['amount']);
            $this->assertArrayHasKey('isAvailable', $item);
            $this->assertInternalType('bool', $item['isAvailable']);
            $this->assertSame($available, ($item['isAvailable']));
            $this->assertArrayHasKey('cost', $item);
            $this->assertNotNull($item['cost']);
        }
    }

    public function testPostNewSoftDrinkWithInvalidName()
    {
        $body = [
            'name' => '',
            'amount' => 3,
            'cost' => 30
        ];
        $this->client->request('POST', '/soft-drinks', [], [], [], json_encode($body, true));

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testPostNewSoftDrinkWithInvalidAmount()
    {
        $body = [
            'name' => 'testname',
            'amount' => -3,
            'cost' => 30
        ];
        $this->client->request('POST', '/soft-drinks', [], [], [], json_encode($body, true));

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testPostNewSoftDrinkWithInvalidCost()
    {
        $body = [
            'name' => 'testname',
            'amount' => 3,
            'cost' => -30
        ];
        $this->client->request('POST', '/soft-drinks', [], [], [], json_encode($body, true));

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testPostNewSoftDrinkWithEmptyBody()
    {
        $body = [];
        $this->client->request('POST', '/soft-drinks', [], [], [], json_encode($body, true));

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testPostNewSoftDrinkWithCorrectData()
    {
        $newSoftDrinkName = 'testname' . random_int(100, 9999);
        $body = [
            'name' => $newSoftDrinkName,
            'amount' => 3,
            'cost' => 30
        ];
        $this->client->request('POST', '/soft-drinks', [], [], [], json_encode($body, true));

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $em = self::bootKernel()->getContainer()->get('doctrine.orm.entity_manager');
        $softDrink = $em->getRepository(SoftDrink::class)
            ->findOneBy(['name' => $newSoftDrinkName]);
        $this->assertNotEmpty($softDrink);
        $em->remove($softDrink);
        $em->flush();
    }


    public function testUpdateSoftDrink()
    {
        $drinkName = 'Green tea';
        $em = self::bootKernel()->getContainer()->get('doctrine.orm.entity_manager');

        /* @var SoftDrink $softDrink */
        $softDrink = $em->getRepository(SoftDrink::class)
            ->findOneBy(['name' => $drinkName]);
        $this->assertNotEmpty($softDrink);

        $body = [
            'name' => $softDrink->getName(),
            'amount' => 10,
            'cost' => 100
        ];
        $this->client->request('PUT', "/soft-drinks/{$softDrink->getId()}", [], [], [], json_encode($body, true));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

    }

    public function testUpdateSoftDrinkWithEmptyBody()
    {
        $drinkName = 'Green tea';
        $em = self::bootKernel()->getContainer()->get('doctrine.orm.entity_manager');

        /* @var SoftDrink $softDrink */
        $softDrink = $em->getRepository(SoftDrink::class)
            ->findOneBy(['name' => $drinkName]);
        $this->assertNotEmpty($softDrink);

        $body = [];
        $this->client->request('PUT', "/soft-drinks/{$softDrink->getId()}", [], [], [], json_encode($body, true));

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

    }

    public function testUpdateSoftDrinkWithInvalidData()
    {
        $drinkName = 'Green tea';
        $em = self::bootKernel()->getContainer()->get('doctrine.orm.entity_manager');

        /* @var SoftDrink $softDrink */
        $softDrink = $em->getRepository(SoftDrink::class)
            ->findOneBy(['name' => $drinkName]);
        $this->assertNotEmpty($softDrink);

        $body = [
            'name' => '',
            'amount' => -10,
            'cost' => -100
        ];
        $this->client->request('PUT', "/soft-drinks/{$softDrink->getId()}", [], [], [], json_encode($body, true));

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

    }
}
