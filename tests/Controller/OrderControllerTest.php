<?php

namespace App\Tests\Controller;

use App\Entity\SoftDrink;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OrderControllerTest extends WebTestCase
{

    public function testMakeNewOrder()
    {
        $client = static::createClient();
        $coffee = 'Coffee espresso';
        $tea = 'Black tea';
        $body = [
            'softDrinks' => [
                $coffee => 1,
                $tea => 2
            ],
            'cash' => [
                '100' => 2,
                '50' => 3,
                '20' => 2,
                '5' => 1
            ]
        ];
        $client->request('POST', '/order', [], [], [], json_encode($body, true));

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $em = self::bootKernel()->getContainer()->get('doctrine.orm.entity_manager');
        /* @var SoftDrink $coffeeDrink */
        $coffeeDrink = $em->getRepository(SoftDrink::class)
            ->findOneBy(['name' => $coffee]);
        $coffeeDrink->setAmount($coffeeDrink->getAmount() + 1);
        $em->persist($coffeeDrink);
        /* @var SoftDrink $teaDrink */
        $teaDrink = $em->getRepository(SoftDrink::class)
            ->findOneBy(['name' => $coffee]);
        $teaDrink->setAmount($teaDrink->getAmount() + 2);
        $em->persist($teaDrink);
        $em->flush();
    }

    public function testGetLastOrder()
    {
        $client = static::createClient();
        $client->request('GET', '/order/last');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

}
